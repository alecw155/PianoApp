import React from 'react';
import {Instruments} from './Util';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stuff: "yes",
    }
  }

  render() {
    return(
      <div className="header">
        <ul>
          <li><a onClick={() => this.props.onClick(Instruments.Piano)}>Piano</a></li>
          <li><a onClick={() => this.props.onClick(Instruments.Organ)}>Organ</a></li>
          <li><a onClick={() => this.props.onClick(Instruments.Synth)}>Synth</a></li>
          <li><a onClick={() => this.props.onClick(Instruments.Drum)}>Drums</a></li>
        </ul>
      </div>
    )
  }
}

export default Header;