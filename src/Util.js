export const Instruments = {
    Piano: "piano",
    Organ: "organ",
    Synth: "synth",
    Drum: "drum",
}