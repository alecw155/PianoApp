import React from 'react';
import logo from './logo.svg';
import './App.css';
import Sidebar from './Sidebar';
import Instrument from './Instrument';
import {Instruments} from './Util';
import Header from './Header';

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current_instrument: Instruments.Organ,
    }
  }

  render() {
    return (
      <div className="Main">
        <Header onClick={(e) => this.setState({current_instrument: e})}/>
        <Instrument instrument={this.state.current_instrument}/>
      </div>
    )
  }
}

function App() {
  return (
    <div className="App">
      <Sidebar />
      <Main />
    </div>
  );
}

export default App;
