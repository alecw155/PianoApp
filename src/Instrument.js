import React from 'react';
import { Instruments } from './Util';
import Piano from './Piano';
import Organ from './Organ';
import Synth from './Synth';
import Drum from './Drum';

class Instrument extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }

    this.renderInstrument = this.renderInstrument.bind(this);
  }

  renderInstrument = () => {
    switch (this.props.instrument) {
      case Instruments.Piano:
        return (<Piano/>);
      case Instruments.Organ:
        return (<Organ/>);
      case Instruments.Synth:
        return (<Synth/>);
      case Instruments.Drum:
        return (<Drum/>);
      default:
        return (<p>Error loading instrument</p>);
    }
  }

  //this is the part where the piano goes
  render() {
    return (
      <div className="instrument">
        {this.renderInstrument()}
      </div>
    )
  }
}

export default Instrument;