import React from 'react';
import Soundfont from 'soundfont-player';
import Keypress from 'keypress';

class Drum extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      stuff: "yes",
    }

    this.Soundfont = require('soundfont-player');
    this.ac = new AudioContext();
    this.playNote = this.playNote.bind(this);
    this.onKeyPressed = this.onKeyPressed.bind(this);
  }

  playNote = (note) => {
    Soundfont.instrument(this.ac, 'synth_drum').then((drum) => {
      drum.play(note);
    })
  }

  onKeyPressed = (e) => {
    console.log(e.keyCode);
  }

  render() {
    var whole_notes = ['C4', 'D4', 'E4', 'F4', 'G4', 'A4', 'B4', 'C5']
    var half_notes = ['C#4', 'D#4', 'F#4', 'G#4', 'A#4']
    var whole_items = []
    var half_items = []

    for (var i = 0; i < whole_notes.length; i++) {
      let j = i;
      let note = whole_notes[i];
      whole_items.push(
        <div className="key white" onClick={() => this.playNote(note)} key={j}>
          <p>{whole_notes[i]}</p>
        </div>
      )
    }

    for (var i = 0; i < half_notes.length; i++) {
      let j = i;
      let note = half_notes[i];
      half_items.push(
        <div className="key black" onClick={() => this.playNote(note)} key={j} id={half_notes[i]}>
          <p>{half_notes[i]}</p>
        </div>
      )
    }

    return(
      <div onKeyPress={(e) => this.onKeyPressed(e)} tabIndex="0" >
      <div className="drum">
        {whole_items}
        {half_items}
        </div>
      </div>
    )
  }
}

export default Drum;